import os, sys, fabio, numpy as np, pylab as plt

fnames =  sys.argv[1:]

d = np.empty((len(fnames), 1679,1475))

for ii in range(len(fnames)):
    d[ii,:,:] = fabio.open(fnames[ii]).data

cmask = np.where(np.sum(d, axis=0)>0, 1, 0)

#plt.imshow(cmask, aspect=1, cmap='bone', origin='lower')
#plt.show()

save = input('Save mask? (Y/N) ')
if save == 'Y':
    done = 0
    while done == 0:
        oname = input('\nEnter filename without extension (e.g. /users/opid15/Masks/test1) ')
        try:
            om = fabio.edfimage.edfimage(cmask)
            om.write('%s.edf' %oname)
            done = 1
        except FileNotFoundError:
            pass
        except KeyboadInterrupt:
            done = 1
plt.close('all')
sys.exit(0)
