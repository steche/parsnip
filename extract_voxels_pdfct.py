import os, sys
from glob import glob
import numpy as np
# from scipy.stats import tmean, sigmaclip
import h5py, argparse
from diffpy.pdfgetx import PDFConfig, PDFGetter
import pylab as plt
import matplotlib.cm as cm

plt.close('all')
plt.ioff()


def flush(message):
    sys.stdout.write("\033[K")
    sys.stdout.write('%s\r' %message)
    sys.stdout.flush()

def get_limits(args, y0):
    dim = y0.shape[-1]
    print('Image size: %d x %d pixels' %(dim, dim))
    cen = (0.5*dim+args.center[0], 0.5*dim+args.center[1])
    print('Horizontal center: %.1f. Vertical center: %.1f' %(cen[0], cen[1]))
    hori, vert = np.arange(dim).reshape(1,-1,1), np.arange(dim).reshape(1,1,-1)
    ref_radius = np.sqrt((cen[1]-hori)**2+(cen[0]-vert)**2)
    # ref_azim = (np.degrees(np.arctan2( (vert-cen[0]), (cen[1]-hori) ))+180).astype(np.int16)
    return dim, ref_radius

def _calc_pdf(q, y):
    pdfgetter = PDFGetter(self.pdf)
    pdfgetter(q, y)
    fq = pdfgetter.getTransformation('fq')
    gr = pdfgetter.getTransformation('gr')
    return fq, gr



parser = argparse.ArgumentParser(description='Extract patterns from a reconstruction')
parser.add_argument('datafiles', type=str, nargs='+',
                     help='Names of input fbp*.h5 files containing the reconstructed data.')
parser.add_argument('-w', '--wavelength', type=float, nargs='?', default=0.123984,
                     help='X-ray wavelength in A. Only used to convert data in r')
parser.add_argument('--sdd', type=float, nargs='?', default=1770.102,
                     help='Sample-detector distance in mm. Only used to convert data in r')
parser.add_argument('-r','--radius', nargs=2, type=float, default=None,
                    help='Inner and outer radius in pixel units of the circular section.\
                          If not specified, default values are 0 and half the recon window')
parser.add_argument('-c', '--center', nargs=2, type=float, default=(0,0),
                    help='Center offset (horizontal and vertical, pixel units) of the selected area.\
                          By default it is the center of rotation.')
parser.add_argument('-t', '--trim', nargs=2, type=int, default=None,
                    help='Lowest and highest percentile used for trimmed mean.\
                          They are integers since the method relies on a list of np.percentile-filtered data.\
                          Default: 50, 98')
parser.add_argument('--normfactor', nargs='?', type=float, default=1e1,
                    help='Multiplicative factor for pattern intensities. Used to avoid rounding errors in further analysis.')
parser.add_argument('--savexy', nargs='?', type=bool, default=False, const=True,
                    help='Save average pattern.')
parser.add_argument('--demo', nargs='?', type=bool, default=False, const=True,
                    help='For the first file analysed, plot ROI and its average.')
parser.add_argument('--pdfcfg', nargs='?', type=str, default='./pdfgetx3.cfg',
                    help='pdfgetx3 config file path')
args = parser.parse_args()

pdfer = None
conf = PDFConfig()
if os.path.isfile(args.pdfcfg):
    conf.readConfig(args.pdfcfg)
    pdfer = PDFGetter()
    pdfer.config = conf


print(args.datafiles)
outnames = []
for f in args.datafiles:
    scano = os.path.dirname(f)[-4:]
    zpos = f.split('scan')[-1].split('pilatus')[0].replace('_','')
    if args.trim == None:
        tstr = 't0'
    else:
        tstr = 't%d_%d' %(args.trim[0], args.trim[1])
    oname = '%s/avgroi_%s_s%s_cx%.1f_cy%.1f_r%.1f_%s' %(os.path.dirname(f), scano, zpos,
                                                     args.center[0], args.center[1],
                                                     args.radius[1], tstr)
    outnames.append(oname)
# outnames = ['%s_avgroi' %(f.split('.')[0]) for f in args.datafiles]
# for i in range(len(outnames)):
#     print(args.datafiles[i], outnames[i])
for i in range(len(args.datafiles)):
    with h5py.File(args.datafiles[i], 'r') as f:
        unit = [k for k in f.keys() if k not in ['reconstruction','mean_diode', 'si']][0]
        x0 = f[unit][()]
        y0 = f['reconstruction'][()]
    if i == 0:
        dim, radius = get_limits(args, y0)
        try:
            inner_radius, outer_radius = args.radius[0], args.radius[1]
        except TypeError:
            inner_radius, outer_radius = 0, dim/4.0
        # roi2d = np.logical_and(np.where(radius>inner_radius, y0, 0), np.where(radius<=outer_radius, y0, 0))
        roi_in = np.where(radius<=outer_radius, y0, np.nan)
        roi_out = np.where(radius>outer_radius, y0, np.nan)
        wu, wv = np.where(radius<=outer_radius)[1], np.where(radius<=outer_radius)[2]
        # print(wu, wv)
    if args.trim == None:
    # approach 1: calculate median along the curve stack
    # roisum = np.median(y0[:,wu,wv], axis=-1); #print(roisum.shape, y0[:,wu,wv].shape)
        roisum = np.average(y0[:,wu,wv], axis=-1); #print(roisum.shape, y0[:,wu,wv].shape)

    # approach 2 : get curves based on 1d percentile and average them
    if args.trim != None:
        ymin, ymax = args.trim[0], args.trim[1]
        ystats = [np.percentile(y0[:,wu,wv], pct, axis=-1) for pct in range(1,101)]
        # ystatplot = False
        if args.demo == True:
            fig0 = plt.figure(figsize=(8,8));
            # ax1 = fig0.add_subplot(221); ax2 = fig0.add_subplot(222)
            # ax3 = fig0.add_subplot(223); ax4 = fig0.add_subplot(224)
            ax1 = fig0.add_subplot(111)
            st = 5 # percentile average step
            for ii in range(0,100,st):
                ploty = np.average(ystats[ii:ii+st], axis=0)
                ploty = ploty+1*ii-ystats[ii][0]
                ax1.plot(x0, ploty, color=cm.jet(int(256*ii/100) ) )
                ax1.text(x0[-1], ploty[-1], '%d-%d' %(ii,ii+st), color=cm.jet(int(256*ii/100) ), ha='right', va='bottom')
            # ax2.plot(range(len(ystats)), [np.sum(ii) for ii in ystats], label='sum')
            # ax3.plot(range(len(ystats)), [np.mean(ii) for ii in ystats], label='mean')
            # ax3.plot(range(len(ystats)), [np.median(ii) for ii in ystats], label='median')
            # # ax4.plot(range(len(ystats)), [np.max(ii) for ii in ystats], label='max')
            # ax4.plot(np.histogram([np.sum(ii) for ii in ystats], bins=len(ystats)//4)[0], label='sum hist')
            # for aa in [ax2,ax3,ax4]:
            #     aa.legend(borderaxespad=0, fontsize=7)
            # plt.show()
        roisum = ystats[ymin:min(100, 1+ymax)]
        roisum = np.average(roisum, axis=0)
    if pdfer:
        pdfer(x0, roisum)
        pdfer.getTransformation('fq')
        pdfer.getTransformation('gr')
    if args.savexy == True:
        outname = '%s.xy' %(outnames[i])
        np.savetxt(outname, np.transpose([x0, roisum]), fmt='%-12.5f')
        print('Saved %s' %outname, end=';')
        outname = '%s.gr' %(outnames[i])
        np.savetxt(outname, np.transpose([pdfer.gr[0], pdfer.gr[1]]), fmt='%-12.5f')
        print('Saved %s' %outname, end=';')
        outname = '%s.fq' %(outnames[i])
        np.savetxt(outname, np.transpose([pdfer.fq[0], pdfer.fq[1]]), fmt='%-12.5f')
        print('Saved %s' %outname, end='\r')
    if args.demo == True and i==0:
        # plt.close('all');

        fig, ax = plt.subplots(4, 1, figsize=(9, 15)); fig.suptitle(args.datafiles[i].split('/')[-1])
        #-- show amorphous intensity : 100, 228, or 275
        ax[0].imshow(roi_out[228,:,:], cmap='binary', origin='lower');
        ax[0].imshow(roi_in[228,:,:], cmap='jet', origin='lower');
        #-- show Si 111 intensity
        # ax[0].imshow(roi_out[131,:,:], cmap='binary', origin='lower');
        # ax[0].imshow(roi_in[131,:,:], cmap='jet', origin='lower');
        #-- show average intensity
        # ax[0].imshow(np.average(roi_out, axis=0), cmap='binary', origin='lower');
        # ax[0].imshow(np.average(roi_in, axis=0), cmap='jet', origin='lower');
        ax[-3].plot(x0, roisum, 'ko-', mfc='none'); ax[-3].set_xlim((1, 8))
        ax[-2].plot(pdfer.fq[0], pdfer.fq[1]);
        ax[-1].plot(pdfer.gr[0], pdfer.gr[1])
        plt.tight_layout()
        plt.show()
        # args.savexy = False
        # sys.exit(0)
