import os, sys, argparse
import h5py
import numpy as np
from glob import glob
from time import time

def flush(message):
    sys.stdout.write("\033[K")
    sys.stdout.write('%s\r' %message)
    sys.stdout.flush()

parser = argparse.ArgumentParser(description='Save all azint*h5 patterns into column files.')
parser.add_argument('datafiles', nargs='+', type=str,
                    help='String(s) passed to system to look for integrated files')
args = parser.parse_args()

odir = '/data/visitor/ch6210/id15/processed/ORIGIN/'
fnames = args.datafiles
maxdiode = 1e8
tis = []
if os.path.isdir(odir) == False:
    os.mkdir(odir)
erlog = '%s/errlog.txt' %odir
outerr = open(erlog, 'w')
outerr.close()
for i in range(len(fnames)):
    t0 = time()
    scano = int(fnames[i].split('scan')[-1][:4]) ;
    with h5py.File(fnames[i], 'r+') as f:
        try:
            tth = f['2th_deg'][()]
        except:
            tth = f['q_A^-1'][()]
        yarr = f['azim_01/intensities'][()]
        times = np.arange(yarr.shape[0]);
        diode = np.ones(yarr.shape[0]) * maxdiode
        try:
            diode = f['positions/fpico3'][()]
        except KeyError:
            h5path = os.path.split(os.path.split(f['info/datafiles'][0].decode('ascii'))[0])[0] ;
            mstr = h5path + '/%s.h5' %(os.path.split(h5path)[-1]) ;
            h5file = glob(mstr)[0]
            with h5py.File(h5file, 'r') as urf:
                diode = urf['%d.1/measurement/fpico3' %scano][()]
                f['positions'].create_dataset('fpico3', data = diod3)
            diode = diod3
        try:
            yj = yarr[:,:] / np.reshape(diode, (-1,1)) * maxdiode
            outarr = np.empty((len(tth), 1+yj.shape[0]))
            outarr[:,0] = tth
            outarr[:,1:] = yj.T
            oname = '%s/%s.xyy' %(odir, os.path.basename(fnames[i]).replace('azint_','columns_').replace('_pilatus','').split('.h5')[0])
            if os.path.isfile(oname) == False:
                np.savetxt(oname, outarr, fmt=['%-.5f']+['%d' for i in range(yj.shape[0])])
                flush('File %3d/%3d saved %s' %(i+1, len(fnames), oname))
            if os.path.isfile(oname) == True:
                flush('File %3d/%3d already there %s' %(i+1, len(fnames), oname))
        except:
            line = '%s %s %s\n' %(fnames[i], yarr.shape, tth.shape)
            with open(erlog, 'a') as outerr:
                outerr.write(line)
    tis.append(time()-t0)
print('\nDone. Processed %d hdf5 files in %.1f s (%.2f files/s)' %(len(fnames), np.sum(tis), 1/np.mean(tis)))
