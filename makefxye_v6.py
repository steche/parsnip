#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, re, sys, math, argparse
import numpy as np
import time
import scipy
from scipy.stats import binned_statistic
import matplotlib.pyplot as plt



def Flushing_Output(message):
    sys.stdout.flush()
    sys.stdout.write('%s\r' %message)
    time.sleep(0.0)


def tailhead(x,y,e, nlines=10, title='Original'):
    print('\n%s x,y,e (%i points):' %(title, len(x)) )
    for i in range(nlines):
        print('%12.5f %12.5f %12.5f' %(x[i], y[i], e[i]))
    for i in range(-nlines,0,1):
        print('%12.5f %12.5f %12.5f' %(x[i], y[i], e[i]))
    print('\n')


def extract_powderpattern_records(powpat):
    ifile = open('%s'%(powpat), 'r')
    ifilelines = ifile.readlines()
    x, y, e = [], [], []
    for index, i in enumerate(ifilelines):
        i = i.strip()
        if re.search('[A-z]{4,}',i) is None and re.search('^[0-9]+',i) is not None:
            i.strip()
            if i.strip():  #exclude empty lines
                rec = i.split()
                if len(rec) == 3 and float(rec[0])>=-20000 and float(rec[1])>=-20000 and float(rec[2])>=-20000:
                        datfmt='3'
                        x.append(float(rec[0]))
                        y.append(float(rec[1]))
                        e.append(float(rec[2]))
                        message='reading ESD from datafile...'
                elif len(rec) == 2 and float(rec[0])>=-20000 and float(rec[1])>=-20000:
                        datfmt='2'
                        x.append(float(rec[0]))
                        y.append(float(rec[1]))
                        e.append(float(0))
                        message='no ESD found in datafile, setting esd to 0...'
                else:
                        print('these lines were skipped:')
                        print(index, len(rec), rec)
#                        pass
    x = np.array(x)
    y = np.array(y)
    e = np.array(e)
    print('\n%s' %(message))
    tailhead(x, y, e, 10, 'Original')
    return x, y, e, datfmt


def data_clip_convert(x, y, e):
    if xi == 'qA':
        if xe == 'qnm':
            x1=10*x; des = 'qye'
        elif xe == 'tth':
            x1=2*180/np.pi*np.arcsin(x*wvln/(4*np.pi)); des = 'xye'
        elif xe == 'ctth':
            x1=200*180/np.pi*np.arcsin(x*wvln/(4*np.pi)); des = 'fxye'
        elif xe == 'qA':
            des = 'qye'
    elif xi == 'qnm':
        if xe == 'qA':
            x1=0.1*x; des = 'qye'
        elif xe == 'tth':
            x1=2*180/np.pi*np.arcsin(0.1*x*wvln/(4*np.pi)); des = 'xye'
        elif xe == 'ctth':
            x1=200*180/np.pi*np.arcsin(0.1*x*wvln/(4*np.pi)); des = 'fxye'
        elif xe == 'qnm':
            des = 'qye'
    elif xi == 'tth':
        if xe == 'qA':
            x1=4*np.pi*np.sin(0.5*np.pi/180*x)/wvln; des = 'qye'
        elif xe == 'qnm':
            x1=40*np.pi*np.sin(0.5*np.pi/180*x)/wvln; des = 'qye'
        elif xe == 'ctth':
            x1=100*x; des = 'fxye'
        elif xe == 'tth':
            des = 'xye'
    elif xi == 'ctth':
        if xe == 'qA':
            x1=4*np.pi*np.sin(0.005*np.pi/180**x)/wvln; des = 'qye'
        elif xe == 'qnm':
            x1=40*np.pi*np.sin(0.005*np.pi/180*x)/wvln; des = 'qye'
        elif xe == 'tth':
            x1=0.01*x; des = 'xye'
        elif xe == 'ctth':
            des = 'fxye'

    ynorm_divisor = np.diff(x1)/(np.diff(x)[0])
    ynorm_divisor = np.append(ynorm_divisor, ynorm_divisor[-1])
    y1 = y/ynorm_divisor
    e1 = e/ynorm_divisor

    ini, fin = np.argmin(abs(x1-args.start)), np.argmin(abs(x1-args.end))
    x1 = x1[ini:fin]
    y1 = y[ini:fin]
    e1 = e[ini:fin]
    
    tailhead(x1, y1, e, 10, 'Unit-converted, intensities corrected for binsize, clipped at limits')
    return x1, y1, e1, des


def data_rebin_rescale(x, y, e, newbin):
    bins = np.arange(float(args.start), float(args.end)+newbin, newbin)
    y1, x1, n = binned_statistic(x, y, bins=bins, statistic='mean')
    e1, x1, n = binned_statistic(x, e, bins=bins, statistic='mean')
    y1 += args.add; y1 *= args.mult
    e1 *= args.mult
    tailhead(x1, y1, e1, 10, 'Current x, y, e')
    return x1, y1, e1


def write_gsas(powpat, des,
               prmfile, x, y, e,
               newbin, write_header):
    ##the following keys are required in line 2 of the GSAS .fxye format.
    bank = 'BANK'
    bintyp = 'CONS'
    tupe = des.upper()
    ##calculate the keys and write the GSAS header lines
    nchan = len(x)                      #count the number of records for the NCHAN field
    nrec = nchan                        #sets the NREC field equal to NCHAN (NOTE: only applies to CONS bintype)
    bcoef1 = x[0];                      #sets BCOEF1 key equal to the first 2theta value
    bcoef2=abs(x[1]-x[0]);              #sets BCOEF2 key equal to the 2theta step in centigrads
    print('NCHAN = NREC = %i (number of channels)' %nchan )
    print('BCOEF1 = %.5f (1st x value in new units)' %bcoef1)
    print('BCOEF2 = %.5f (x step in new units)' %bcoef2)
    headline1 = 'Instrument parameter       %s'%(prmfile);'{{0: <80{}}}'.format(headline1)
    headline2 = '%s 1 %i %i %s %.6f %.6f 0 0 %s' %(bank,nchan,nrec,bintyp,bcoef1,bcoef2,tupe);'{{0: <80{}}}'.format(headline2)
    oname = '%s.%s'%(re.sub(r'.[A-z]*$','',powpat),des)
    endfile = open(oname, 'w')
    if write_header == True:
        endfile.writelines('%s\n' %(headline1))
        endfile.writelines('%s\n' %(headline2))
    for riga in zip(x,y,e):
        eins = "{:.6f} {:.6f} {:<80.6f}".format(riga[0],riga[1],riga[2])
        zwei = '%s\n'%(eins[:80])
        endfile.writelines(zwei)
    endfile.close()
    elapsed_time = time.time()-start_time
    print('\n%s written' %oname)
    print('Time elapsed: %.2f s\n' %elapsed_time)


def write_normal(x, y, e, des, powpat):
    oname = '%s.%s'%(re.sub(r'.[A-z]*$','',powpat),des)
    endfile = open(oname, 'w')
    if np.mean(e) != 0.0:
        for riga in zip(x, y, e):
            eins = "{:.5f} {:.5f} {:.5f}".format(riga[0], riga[1], riga[2])
            zwei = '%s\n'%(eins)
            endfile.writelines(zwei)
    elif np.mean(e) == 0.0:
        for riga in zip(x, y):
            eins = "{:.5f} {:.5f}".format(riga[0], riga[1])
            zwei = '%s\n'%(eins)
            endfile.writelines(zwei)
    endfile.close()
    print('\n%s written' %oname)


##########execution
start_time = time.time()
#enter name of 1) powder pattern and 2) instrument parameter file (*.prm as required by GSAS) in the command-line
# example: python makefxye.xy ceo2.xye --xi=tth --xe=tth --start=1.2 --end=48.3 -b 0.006
parser = argparse.ArgumentParser()
parser.add_argument('powpat',metavar='P', type=str, help="pattern filename")
parser.add_argument("--xi", help="data format: qA (Q in Angstroms), qnm (Q in nm), tth (standard 2theta)", default='tth', choices=['qA','qnm','tth'])
parser.add_argument("--xe", help="final data format: qA (Q in Angstroms), qnm (Q in nm), tth (standard 2theta), ctth (2theta centigrads)", 
                    default='tth', choices=['qA','qnm','tth','ctth'])
parser.add_argument("-l", help="radiation wavelength used in Angstroms", type=float, default=1.5406)
parser.add_argument("-b", help="binsize in the FINAL units", type=np.float64, default=0.01)
parser.add_argument("--start", help="initial X value in output file", type=float, default=None)
parser.add_argument("--end", help="final X value in output file", type=float, default=None)
parser.add_argument("--gsas", help="write header for GSAS (xe must be ctth). True or False", type=bool, default=False)
parser.add_argument('-p', metavar='p', type=str, help="instrument parameter filename for GSAS (e.g. id31.prm)", default='id31.prm')
parser.add_argument('-a', '--add', type=float, help="additive constant to rebinned pattern", default=0)
parser.add_argument('-m', '--mult', type=float, help="multiplicative constant to rebinned pattern", default=1)

global args
args = parser.parse_args()

powpat = sys.argv[1]
prmfile = args.p
write_header = args.gsas
xi = args.xi
xe = args.xe
wvln = args.l

x0, y0, e0, datfmt = extract_powderpattern_records(powpat)
args.start, args.end = (float(args.start) or x0[0]), (float(args.end) or x0[-1])
x1, y1, e1, des = data_clip_convert(x0, y0, e0)
oldbin = np.mean(np.diff(x0))
newbinvar = np.mean(np.diff(x1))
newbincon = args.b+1e-10
print('old binsize (old x units)= %.6f (mean if not constant)' %oldbin)
print('old binsize (new x units)= %.6f (mean if not constant)' %newbinvar)
print('new binsize (new x units)= %.6f (constant)' %newbincon)
if np.divide(oldbin, newbincon) > 1.0:
    print('\nWARNING: you are using a binsize smaller than the binsize in the original data!\n')
x2, y2, e2 = data_rebin_rescale(x1, y1, e1, newbincon)
if write_header == True:
    write_gsas(powpat, des, prmfile,
               x2, y2, e2, newbin, write_header)
else:
    write_normal(x2, y2, e2, des, powpat)





















