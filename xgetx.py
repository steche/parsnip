import os, sys, argparse
from glob import glob
import numpy as np
import h5py
import pylab as plt
plt.ioff()


def norm01(x):
#    return (x-min(x))/(max(x)-min(x))
    return x/max(x)

parser = argparse.ArgumentParser(description='Save all azint*h5 patterns into ascii and column files.')
parser.add_argument('datafiles', nargs='+', type=str,
                    help='String(s) passed to system to look for integrated files')
parser.add_argument('-n', nargs=1, type=str, default='*',
                    help='Specify number to extract only one pattern. Default: * = extract all')
args = parser.parse_args()

maxdiode = 1e8
save = False

fnames = args.datafiles
if len(fnames) == 0:
    print('## No files found!')
    sys.exit(0)
#print(fnames)


for fname in fnames:
    mcname = fname.replace('_pilatus.h5','.xyy').replace('azint_','columns_')
#    if os.path.isfile(mcname):
#        continue
    n = '*'
    with h5py.File(fname, 'r') as f:
        try:
            diode = f['positions/fpico3'][()]
        except:
            print('## Diode not available. Replaced by ones')
            diode = np.ones(ydata.shape[0])*1e7
        xunit = [i for i in f.keys() if 'azim' not in i and i not in ['info','positions']][0]
        x = f[xunit][()]
        ydata = f['azim_01/intensities'][()]
        #print('## diode array length:', len(diode), 'data array length:', ydata.shape[0])
        if len(diode) - ydata.shape[0] >= 1:
            diode = np.interp(np.arange(ydata.shape[0]), np.arange(len(diode)), diode)
        try:
            ydatasc = ydata / np.reshape(diode, (-1,1)) * maxdiode
        except ValueError:
            continue
        print('## Array shape', f['azim_01/intensities'].shape)
        try:
            n = int(args.n)
            y = ydatasc[n,:]
        except IndexError:
            print('## Out of bounds. Defaulted to 0')
            n = 0
            y = ydatasc[n,:]
        except ValueError:
            n = '*'
            yy = ydatasc[:,:]
    plt.close('all')
    fig, ax = plt.subplots(1,2, figsize=(6,3))
#    plt.ion(); plt.show()
    ax[0].plot(np.arange(ydata.shape[0]), norm01(np.mean(ydata, axis=-1)), 'k-', label='unscaled')
    ax[0].plot(np.arange(ydata.shape[0]), norm01(np.mean(ydatasc, axis=-1)), 'g-', label='scaled')
    ax[1].plot(np.arange(diode.shape[0]), diode*66595, label='photons/s')
    for aa in ax:
        aa.legend(borderaxespad=0.1, fontsize=10, loc=0)
        aa.set_xlabel('Pattern number')
        aa.grid('both')
    fig.suptitle(fname.split('azint_')[-1].split('_pilatus')[0])
    fig.tight_layout()
    figname = '/data/visitor/ch6210/id15/ORIGIN_NEW/flux_int_%s.png' %(fname.split('azint_')[-1].split('_pilatus')[0])
    plt.savefig(figname, dpi=80)
    print('## Saved %s' %(figname), end='\r')

#    plt.draw()
#    plt.pause(0.001)

    if save == True:
        if n == '*':
            outarr = np.empty((len(x), 1+ydatasc.shape[0]))
            outarr[:,0] = x
            outarr[:,1:] = ydatasc.T
            print('## Saved %s' %mcname)
            if os.path.isfile(mcname) == False:
                np.savetxt(mcname, outarr, fmt=['%-.5f']+['%d' for i in range(ydatasc.shape[0])])
            for n in range(yy.shape[0]):
                print('## Saved file %d/%d' %(n+1, yy.shape[0]), end='\r')
                xyname = fname.replace('pilatus.h5','%04d.xy' %n).replace('azint_','y_')
                np.savetxt(xyname, np.transpose([x,yy[n,:]]), fmt='%.5f')
            #command = 'pdfgetx3 %s %s --plot None --force yes' %(xyname.replace('%04d' %n, '*'), wargs)
            #print('##%s' %command)
            #os.system(command)
        else:
            xyname = fname.replace('pilatus.h5','%04d.xy' %n)
            print('## Saved file 1/1', end='\r')
            np.savetxt(xyname, np.transpose([x,y]), fmt='%.5f')
            #command = 'pdfgetx3 %s %s' %(xyname, wargs)
            #print('## %s' %command)
            #os.system(command)
    print('\n## Done')
        #plt.ioff()





