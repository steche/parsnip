import os, sys
from glob import glob
import numpy as np
import h5py
import pylab as plt
plt.ioff()


maxdiode = 1e8

if os.path.isfile(sys.argv[1]) == False:
    print('## %s not found' %sys.argv[1])
    sys.exit(0)
else:
    fname = sys.argv[1]

if len(sys.argv) > 2:
    wargs = sys.argv[2:]
    keys = wargs[::2]
    vals = wargs[1::2]
    wargs = []
    for ii in range(len(vals)):
        wargs.append(' '.join([keys[ii], vals[ii]]))
    wargs = ' '.join(wargs)
else:
    wargs = ''

n = 0

with h5py.File(fname, 'r') as f:
    try:
        diode = f['positions/fpico3'][()]
    except:
        print('Diode not available. Replaced by ones')
        diode = np.ones(ydata.shape[0])*1e7
    xunit = [i for i in f.keys() if 'azim' not in i and i not in ['info','positions']][0]
    x = f[xunit][()]
    ydata = f['azim_01/intensities']
    print('diode array length:', len(diode), 'data array length:', ydata.shape[0])
    if len(diode) - ydata.shape[0] >= 1:
        diode = np.interp(np.arange(ydata.shape[0]), np.arange(len(diode)), diode)
    ydatasc = ydata / np.reshape(diode, (-1,1)) * maxdiode
    print('## Array shape', f['azim_01/intensities'].shape)
    try:
        n = int(input('## Pattern number? (default:%d, * for all) ' %n))
#        y = ydata[n,:] / diode[n] * maxdiode
        y = ydatasc[n,:]
    except IndexError:
        print('## Defaulted to 0')
        n = 0
#        y = f['azim_01/intensities'][n,:] / diode[n] * maxdiode
        y = ydatasc[n,:]
    except ValueError:
#        yy = f['azim_01/intensities'][:,:] / np.reshape(diode,(-1,1)) * maxdiode
        yy = ydatasc[:,:]
        case = 2
        n = '*'
    fig, ax = plt.subplots(1,2)
    ax[0].plot(np.arange(ydata.shape[0]), np.mean(ydata, axis=-1), 'k-', label='unscaled')
    ax[0].plot(np.arange(ydata.shape[0]), np.mean(ydatasc/maxdiode*max(diode), axis=-1), 'g-', label='scaled')
    ax[1].plot(np.arange(diode.shape[0]), diode)
plt.show()


if n == '*':
    for n in range(yy.shape[0]):
        print(n+1, end='\r')
        xyname = fname.replace('pilatus.h5','%04d.xy' %n)
        np.savetxt(xyname, np.transpose([x,yy[n,:]]), fmt='%.5f')
    command = 'pdfgetx3 %s %s --plot None --force yes' %(xyname.replace('%04d' %n, '*'), wargs)
    print('##%s' %command)
    os.system(command)
else:
    xyname = fname.replace('pilatus.h5','%04d.xy' %n)
    np.savetxt(xyname, np.transpose([x,y]), fmt='%.5f')
    command = 'pdfgetx3 %s %s' %(xyname, wargs)
    print('## %s' %command)
    os.system(command)
print('##Done')
plt.close('all')
