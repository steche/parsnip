#!/users/opid15/miniconda3/bin/python
import os, sys, h5py, argparse, re
import numpy as np
import pyqtgraph as pg
from PyQt5 import QtGui, QtCore
from glob import glob
import time
from eclab_reader import eclog

pg.setConfigOption('leftButtonPan', False)


def dtconv(silxtime):
    tst = time.mktime(time.strptime(silxtime, '%Y-%m-%dT%H:%M:%S.%f%z'))
    return tst

def mouseMoved(evt):
    mousePoint = plt.vb.mapSceneToView(evt[0])
    if elog == True:
        label.setText("<span style='font-size: 14pt; color: white'> time (%s):%.0f, \
                      <span style='color:yellow'> scan:%d, \
                      <span style='color:cyan'> E (V):%.3f, \
                      <span style='color:#ff8b00'> I (A):%.3f, \
                      </span>" %(args.unit, mousePoint.x(),
                      scano[np.argmin(abs(times-mousePoint.x()))],
                      ec_E[np.argmin(abs(ec_t-mousePoint.x()))],
                      ec_I[np.argmin(abs(ec_t-mousePoint.x()))]))
    else:
        label.setText("<span style='font-size: 14pt; color: white'> time (%s):%.2f, \
              <span style='color:yellow'> scan:%d" %(args.unit, mousePoint.x(),
              scano[np.argmin(abs(times-mousePoint.x()))]))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get h5 scans timestamps.Example: \
                                     lid15agpu1:inhouse4/ihma109/id15 % timeseries ACL9011001d --log Logs/ACL9011001d_C01.mpt -u h')
    parser.add_argument('datadir', nargs='?', type=str,
                        help='High level (sample) dir with scan data')
    parser.add_argument('-u','--unit', default='s', help='Display time units', choices=['s','min', 'h'])
    parser.add_argument('--log', help='relative path to EC-Lab .mpt log file', default=None)
    parser.add_argument('--out', help='output file without extension. *.scans: time vs scan number; *.eclab: time vs ECLab reading', default=None, type=str)
    args = parser.parse_args()
    fname = re.sub('/$', '', args.datadir)
    tgt = sorted(glob('%s/%s_*/%s_*.h5' %(fname, fname.split('/')[-1], fname.split('/')[-1])))
    times = np.empty(len(tgt))
    for i in range(len(tgt)):
        with h5py.File(tgt[i],'r') as f:
            try:
                k = sorted(f.keys())
                t0 = f['%s/start_time' %(k[0])][()].decode('ascii')
                times[i] = dtconv(t0)
            except:
                print(i, end=' ')
                times[i] = times[i-1]
    print('skipped')

    times -= min(times)
    scano = range(1, 1+len(tgt))
    elog = False
    if args.log != None and os.path.isfile(args.log):
        elog = True
        ec_t, ec_E, ec_I = eclog(args.log)
        ec_I = ec_I*1e-3

    if args.unit == 's':
        pass
    elif args.unit == 'min':
        times = times/60
        if elog == True:
            ec_t = ec_t/60
    elif args.unit == 'h':
        times = times/3600
        if elog == True:
            ec_t = ec_t/3600

    app = QtGui.QApplication([])
    win = pg.GraphicsWindow(title='%s scan times' %(args.datadir))
    label = pg.LabelItem(justify = "right")
    win.addItem(label)
    plt = win.addPlot(row=1, col=0, name='p1')
    proxy = pg.SignalProxy(plt.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
    plt.showGrid(True, True, 0.5)
    li = pg.ScatterPlotItem(x=times, y=scano, symbol='o', pen=pg.mkPen('y', width=2))
    plt.addItem(li)

    if elog == True:
        plu = win.addPlot(row=2, col=0, name='p2')
        plu.showGrid(True, True, 0.5)
        lec = pg.ScatterPlotItem(x=ec_t, y=ec_E, symbol='t', pen=pg.mkPen('c', width=1))
        plu.addItem(lec)
        lec = pg.ScatterPlotItem(x=ec_t, y=ec_I, symbol='s', pen=pg.mkPen('#ff8b00', width=1))
        plu.addItem(lec)
        plu.setXLink('p1')
        plt.setXLink('p2')

    if args.out != None:
        np.savetxt('%s.scans' %args.out, np.transpose([times, scano]), fmt='%-12.5f')
        np.savetxt('%s.eclab' %args.out, np.transpose([ec_t, ec_E, ec_I]), fmt='%-12.5f')

    pg.QtGui.QApplication.processEvents()
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
