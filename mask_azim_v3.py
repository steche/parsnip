#!/users/opid15/miniconda3/bin/python
import sys, os
import fabio, h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pyFAI
import warnings

warnings.filterwarnings('ignore')

plt.logging.disable(level=50)
plt.ion()
#%%
def remask(ai, phimin, phimax, qmax, qmin):
# phimin, phimax, qmax, qmin = -180, 30, 5, 0
    phi = np.degrees(ai.chiArray())
    phi_q_mask = np.zeros(phi.shape)
    phi_lo = np.where(phi<phimin, 1, 0); print('Phi min: %d points masked' %(len(np.where(phi<phimin)[0])), end='\n')
    phi_hi = np.where(phi>phimax, 1, 0); print('Phi max: %d points masked' %(len(np.where(phi>phimax)[0])), end='\n')
    phi_q_mask += phi_lo
    phi_q_mask += phi_hi
    qmap = ai.qArray()*0.1
    qmap_hi = np.where(qmax<qmap, 1, 0); print('Q max: %d points masked' %(len(np.where(qmax>qmap)[0])), end='\n')
    qmap_lo = np.where(qmap<qmin, 0, 1); print('Q min: %d points unmasked' %(len(np.where(qmap<qmin)[0])))
    phi_q_mask += qmap_hi
    phi_q_mask *= qmap_lo
    return np.where(phi_q_mask>0, 1, 0)
#%%

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Build azimuthally resolved mask')
    parser.add_argument('image', type=str,
                        help='File name of data image to show mask onto')
    parser.add_argument('-i', '--index', type=int, default=0,
                        help='Image number in hdf5 image file')
    parser.add_argument('-p','--poni', type=str,
                        help='Calibration file name')
    parser.add_argument('-m','--mask', type=str,
                        help='File name of base mask (detector gaps, beamstop, bad pixels)')
    parser.add_argument('--phi_lo',  type=float, default=-180,
                        help='Lower azimuthal angle in the cake. 0=E, -180=180=W, -90=N, 90=S')
    parser.add_argument('--phi_hi', type=float, default=180,
                        help='Higher azimuthal angle in the cake. 0=E, -180=180=W, -90=N, 90=S')
    parser.add_argument('--qmax', type=float, default=30,
                        help='Mask above this Q [1/A] value')
    parser.add_argument('--qmin', type=float, default=0,
                        help='Unmask below this Q [1/A] value')
    parser.add_argument('-c','--chi', type=int, choices=[0,1], help='ChiDisc as defined in pyFAI. 0=Zero, 1=Pi', default=1)
    parser.add_argument('--gui', type=int, choices=[0,1], help='Plot image and mask. 0=No GUI, 1=Show', default=1, const=1)
    args = parser.parse_args()
    #-- show help if no arguments
    #    if len(sys.argv)==1:
    #        parser.print_help(sys.stderr)
    #        sys.exit(1)

    if args.image.split('.')[-1] in ['cbf','edf','tif','tiff']:
        baseim = fabio.open(args.image).data
    elif args.image.split('.')[-1] in ['h5'] and args.image.split('/')[-1].startswith('pilatus'):
        with h5py.File(args.image, 'r') as pf:
            ds = pf['entry_0000/measurement/data'][()].squeeze()
            print('Image shape:', ds.shape, end='')
            if len(ds.shape) == 2:
                baseim = np.log(ds+2)
            elif len(ds.shape) == 3 and type(args.index) == int:
                baseim = np.log(ds[args.index].squeeze()+2)
                print(' -->', baseim.shape)
    else:
        print('Something wrong with the image file. Check', args.image.split('.'))
        sys.exit(0)
    basemask = fabio.open(args.mask).data
    print('Mask shape:', basemask.shape)
    ai = pyFAI.load(args.poni)
    phimin, phimax, qmax, qmin, chidisc = args.phi_lo, args.phi_hi, args.qmax, args.qmin, args.chi
    if args.chi == 1:
        ai.setChiDiscAtPi()
    elif args.chi == 0:
        ai.setChiDiscAtZero()
    phi_q_mask = remask(ai, phimin, phimax, qmax, qmin)
    total_mask = np.logical_or(basemask, phi_q_mask)

    cont = 1
    if args.gui == 1:
        plt.close('all')
        fig, ax = plt.subplots(1,1, figsize=(8,8))
        a0 = ax.imshow(baseim, vmin=np.percentile(baseim,10), vmax=np.percentile(baseim,90), cmap='cividis', interpolation='none', zorder=0)
        a1 = ax.imshow(basemask, vmin=0, vmax=1, cmap='cool', alpha=0.5*basemask, zorder=1, interpolation='none')
        a2 = ax.imshow(phi_q_mask, vmin=0, vmax=1, cmap='jet', alpha=0.5*phi_q_mask, zorder=2, interpolation='none')

        while cont == 1:
            what = input('Edit (E), Save (S), ChangeChidDisc (C) or Quit (Q)? ')
            if what in ['C','c']:
                val = -1
                while val not in [0,1]:
                    val = int(input('Chi discontinuity at Pi (1) or Zero (0)'))
                    if val == 1:
                        ai.setChiDiscAtPi()
                    elif val == 0:
                        ai.setChiDiscAtZero()
            if what in ['E','e']:
                vals = input('Input new <phi_min> <phi_max> <qmax> <qmin>(current %.2f %.2f %.2f %.2f) ' %(phimin, phimax, qmax, qmin)).split()
                tgts = ['phimin','phimax','qmax','qmin']
                for i in range(len(vals)):
                    try:
                        vars()[tgts[i]] = float(vals[i])
                    except:
                        print(tgts[i], 'No change')
                phi_q_mask = remask(ai, phimin, phimax, qmax, qmin)
                # total_mask = np.logical_or(basemask, phi_q_mask)
                total_mask = np.where(basemask+phi_q_mask, 1, 0).astype(np.int8)
                a2.set_data(np.where(phi_q_mask>0, 1, 0))
                a2.set_alpha(0.5*phi_q_mask)
                fig.canvas.draw()
                fig.canvas.flush_events()
            elif what in ['S','s']:
                prefix = input('Enter file prefix (no path, no extension) ')
                ofiln = '%s/%s.edf' %(os.getcwd(), prefix)
                ofild = fabio.edfimage.EdfImage(data=total_mask.astype(np.uint8), header=None)
                ofild.write(ofiln)
                print('Mask saved as %s' %ofiln)
                #cont = 0
            elif what in ['Q','q']:
                cont = 0

    elif args.gui == 0:
        prefix = input('Enter file prefix (no path, no extension) ')
        ofiln = '%s/%s.edf' %(os.getcwd(), prefix)
        ofild = fabio.edfimage.EdfImage(data=total_mask.astype(np.uint8), header=None)
        ofild.write(ofiln)
        print('Mask saved as %s' %ofiln)
        sys.exit(0)
