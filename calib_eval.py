#!/users/opid15/miniconda3/bin/python
import os, sys, pyFAI, fabio, argparse, h5py
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl
from matplotlib.colors import LogNorm
from pyFAI.method_registry import IntegrationMethod, Method

plt.logging.disable()
mpl.logging.disable()
plt.close('all')
plt.ioff()

def fwd(x):
    return x**(1/2)
def bwd(x):
    return x**2

def make_azims(azim_min, azim_max, nb_slices, show=False):
    pars = {}
    pars['cake'] = ( float(azim_min), float(azim_max) )
    pars['npieces'] = int(nb_slices)
    #-- calculate azim slices
    if pars['npieces'] == 1:
        pars['azims'] = [( pars['cake'][0], pars['cake'][1] )]
    #-- calculate azim slices if n.pieces > 1
    elif pars['npieces'] > 1:
        vals = np.linspace( pars['cake'][0],
                            pars['cake'][1],
                            pars['npieces']+1 )
        pars['azims'] = [ (vals[i],vals[i+1]) for i in range(pars['npieces']) ]
    if show == True:
        print('\nUsing %i azimuthal slices:' %(len(pars['azims'])))
        for i in pars['azims']:
            print('%.3f - %.3f' %(i[0],i[1]))
        print('______________')
        print('|     -90     |')
        print('|      |      |')
        print('|      |      |')
        print('180 -- o -- 0/-180')
        print('|      |      |')
        print('|      |      |')
        print('|_____ 90 ____|')
    return np.array(pars['azims'])


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Build azimuthally resolved mask')
    parser.add_argument('--image', type=str,
                        help='File name of data image to show mask onto')
    parser.add_argument('--poni', type=str,
                        help='Calibration file name')
    parser.add_argument('--mask', type=str,
                        help='File name of base mask (detector gaps, beamstop, bad pixels)')
    parser.add_argument('--azim', type=str, nargs=3, default=(-180, 180, 20),
                        help='Azimuthal range: lower end of the first slice, \
                        higher end of the last slice, number of azimuthal slices. Default: -180 180 20')
    args = parser.parse_args()

    print('Using image file %s' %args.image)
    print('Using calibration file %s' %args.poni)
    print('Using mask file %s' %args.mask)

    det = pyFAI.detector_factory("/users/opid15/id15_pilatus.h5")
    mask = np.ascontiguousarray(fabio.open(args.mask).data, np.int8)
    det.mask = mask
    ai = pyFAI.load(args.poni)
    ai.detector = det
    meth1 = Method(dim=1, split='full', algo='csr', impl='ocl', target=(0,0))
    #meth1 = Method(dim=1, split='full', algo='csr', impl='cython', target=None) #BUG!
    meth2 = Method(dim=2, split='full', algo='csr', impl='ocl', target=(0,0))
    print(meth1)
    print(meth2)

    try:
        args.azim = [float(args.azim[i]) for i in range(3)]
    except:
        args.azim = [-180, 180, 20]
        print('Error in azim argument. 3 numbers required. Defaulted to -180 180 1')
    azims = make_azims(args.azim[0], args.azim[1], args.azim[2], show=False)
    nabins = len(np.arange(azims[0][0], azims[-1][-1],1))
    ext = args.image.split('.')[-1]
    if ext in ['cbf','edf','tiff']:
        img = fabio.open(args.image).data
    elif ext in ['h5','hdf5']:
        with h5py.File(args.image, 'r') as f:
            try:
                img = f['entry_0000/measurement/data'][()].squeeze()
            except KeyError:
                for scan in f.keys():
                    try:
                        img = f['%s/measurement/pilatus' %scan][()].squeeze()
                    except:
                        pass
            except:
                print('ERROR. Check %s or the script' %args.image)
                sys.exit(0)
        if len(img.shape) == 3:
            img = np.average(img, axis=0);
        elif img.shape == (1679,1475):
            img = img

    norm = mpl.colors.Normalize(vmin=azims[0][0], vmax=azims[-1][-1])
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cm.jet)
    cmap.set_array([])

    ndata = len(azims)
    colors = [cm.jet(0)]
    for i in range(ndata):
        colors.append( cm.jet( int((256./ndata)*(i)) ) )

    fig = plt.figure(facecolor='w', figsize=(16,9))
    #fig.canvas.set_window_title('calib_eval.py :: %s' %(args.image.split('.')[0]))
    ax, bor = [], 0.05
    hei3 = (1-4*bor)/3   #height of the small subplots
    a0 = fig.add_axes([bor, bor, 0.5-bor, 1-1.5*bor])  # le, bo, wi, he
    a0.set_ylabel('azimuthal angle / deg')
    a0.set_xlabel(r'$Q$ / $\mathrm{\AA^{-1}}$')
    a1 = fig.add_axes([0.5+bor, bor, 0.5-1.8*bor, hei3])
    a1.set_xlabel(a0.get_xlabel())
    a1.set_ylabel('integrated intensity / counts')
    a2 = fig.add_axes([0.5+bor, 3*bor+2*hei3, 0.5-1.8*bor, hei3])
    a2.set_xlabel(a0.get_xlabel())
    a3 = fig.add_axes([0.5+bor, 2*bor+hei3, 0.5-1.8*bor, hei3])
    ax.append(a0); ax.append(a1); ax.append(a2); ax.append(a3)
    res = ai.integrate2d(img, npt_azim=nabins, npt_rad=2048, unit='q_A^-1',
                         azimuth_range=(azims[0][0], azims[-1][-1]),
                         polarization_factor=0.99, correctSolidAngle=False,
                         method=meth2)
    ishow = np.nan_to_num(res.intensity, nan=.1, posinf=.1, neginf=.1)+0.1
    vmin = np.percentile(ishow, 5)
    vmax = np.percentile(ishow, 95)
    xlims = (res.radial[0],res.radial[-1])
    ax[0].imshow(ishow, interpolation='none', cmap='gnuplot', aspect='auto',
                 norm=LogNorm(vmin=vmin, vmax=vmax), origin='lower',
                 extent=[xlims[0],xlims[1],res.azimuthal[0],res.azimuthal[-1]])
    for ind, azim in enumerate(azims):
        x, y = ai.integrate1d(img, npt=2048, unit='q_A^-1',
                              polarization_factor=1.05, correctSolidAngle=False,
                              azimuth_range=(min(azim), max(azim)),
                              method=meth1,
                              radial_range=xlims)
        ymask = ma.masked_invalid(ma.masked_where(y<1, y))
        ax[1].plot(x, y+.1, color=colors[ind], label='%d %d' %(min(azim), max(azim)))
        for pos in azim:
            ax[0].axhline(pos, color=colors[ind], ls=':')
        print(ind, 'azim_range=', azim, ' n_valid=', ma.count(ymask))
    ax[1].set_yscale('function', functions=(fwd, bwd))
    for aax in ax[1:]:
        aax.set_xlim((res[1][0],res[1][-1]))
    # only one curve in ax2
#    n_valid = [np.count_nonzero(res.intensity[:,i])/nabins for i in range(len(res.radial))]
#    stdev2d = ma.std(ma.masked_invalid(res.intensity), axis=0)
    # one curve per azim sector in ax2
    wohin = np.digitize(res.azimuthal, bins=sorted(set(azims.flatten())))
    slabs, hits = {}, {}
    for i in range(len(azims)):
        slabs[str(i)] = res.intensity[np.where(wohin==(i+1))]
        hits[str(i)] = len(np.nonzero(slabs[str(i)])[0])
        yvalid = np.sum(np.where(slabs[str(i)]>0, 1, 0), axis=0)/slabs[str(i)].shape[0]
        ax[2].plot(x, yvalid, color=colors[i])
    #
#    ax[2].plot(x, n_valid, 'k.-', zorder=4, alpha=0.5, markersize=2)
#    ax[3].plot(x, stdev2d, 'g-', zorder=2, alpha=0.5)
#    ax[3].set_yscale('log')

#    clb = fig.add_axes([1-3*bor, 0.6-bor, 2*bor, 0.015])
#    plt.colorbar(cmap, cax=clb, orientation='horizontal', ticks=np.linspace(azims[0][0], azims[-1][-1],4))
#    clb.set_title(a0.get_ylabel(), fontsize=10)
#    clb = fig.add_axes([1-3*bor, 0.6-bor, 2*bor, 6*0.015])
#    clb.yaxis.set_visible(False)
#    clb.xaxis.set_visible(False)
#    clob = clb.imshow(180/np.pi*ai.chiArray(ai.detector.shape), norm=norm, cmap='jet')
#    plt.colorbar(clob, ax=clb)
#    plt.colorbar(cmap, cax=clb, orientation='horizontal', ticks=np.linspace(azims[0][0], azims[-1][-1],4))
#    clb.set_title(a0.get_ylabel(), fontsize=10)
    leg = ax[1].legend(borderaxespad=0, loc='upper right')
    for l in leg.get_lines():
        l.set_lw(3)
    ax[2].set_ylabel('valid pixels per radial bin')
    ax[3].set_ylabel('stdev over azim slices / counts', color='g')
    plt.show()
