#!/users/opid15/miniconda3/bin/python
import sys, h5py, fabio, os
from glob import glob
import numpy as np

cont = 0

if len(sys.argv) == 1 or 'help' in sys.argv[:]:
    print('\nUsage: calibrant_cbf.py  scan_level_filename.h5  <scan_nb>\n')
    sys.exit(0)
elif len(sys.argv) == 2:
    tgt = glob('*%s*' %sys.argv[1])
    if len(tgt) == 0:
        tgt = glob('%s' %sys.argv[1])
    scan = '1'
elif len(sys.argv) == 3:
    tgt = glob('*%s*' %sys.argv[1])
    if len(tgt) == 0:
        tgt = glob('%s' %sys.argv[1])
    scan = sys.argv[2]

if len(tgt) == 0:
    print('Input file not found.')
    sys.exit(0)
elif len(tgt) == 1:
    tgt = tgt[0]
    cont = 1
    print('Found %s' %tgt)
elif len(tgt) > 1:
    print('Multiple input files found. Using %s' %(tgt[0]))
    for i in tgt:print(i)
    tgt = tgt[0]
    cont = 1
    

if cont == 1:
    with h5py.File(tgt, 'r') as f:
        im = f['%s.1/measurement/pilatus' %scan][()].squeeze()
        if len(im.shape) == 3:
            #print(im.shape)
            print('Averaging %d frames' %im.shape[0])
            im = np.average(im, axis=0); 
        elif im.shape == (1679,1475):
            im = im
        print(im.shape)
        ene = np.round(1e3*f['%s.1/instrument/positioners/llen' %scan][()], 0)
        ddx = np.round(f['%s.1/instrument/positioners/ddx' %scan][()], 0)
        ddy = np.round(np.ceil(f['%s.1/instrument/positioners/ddy' %scan][()]), 0)
        ddz = np.round(np.ceil(f['%s.1/instrument/positioners/ddz' %scan][()]), 0)
    outim = fabio.cbfimage.cbfimage(im)
    oname = 'Cr2O3_%deV_x%dy%dz%d.cbf' %(ene,ddx,ddy,ddz)
    outim.write('%s/%s' %(os.path.abspath('.'), oname))
    print('Saved %s/%s' %(os.path.abspath('.'), oname))





