#!/users/opid15/miniconda3/bin/python

import os, sys, time, string, datetime
import chardet, codecs
import numpy as np
from PyQt5 import QtGui, QtCore
import pyqtgraph as pg
pg.setConfigOption('leftButtonPan', False)


def guess_encoding(filename):
    u = chardet.UniversalDetector()
    u.reset()
    for line in open(sys.argv[1], 'rb'):
       u.feed(line)
       if u.done: break
    u.close()
    print(u.result)


#def dtconv(epoch):
#    print(epoch)
#    tst = time.mktime(time.strptime(epoch, '%m/%d/%Y %H:%M:%S.%f'))    
#    return tst

def dtconv(tt):
    tt = int(float(tt))
    return tt
#    return str(datetime.datetime.fromtimestamp(tt).strftime('%m/%d/%Y %H:%M:%S.%f'))


def mouseMoved(evt):
    mousePoint = plt.vb.mapSceneToView(evt[0])
    label.setText("<span style='font-size: 14pt; color: white'> time (s):%d, <span style='color: white'> Y:%.2f</span>" %(mousePoint.x(), mousePoint.y()))


#if len(sys.argv) <2:
#    print('Usage: python eclab_reader.py filename.mpt')
#    sys.exit(0)


def eclog(fname):
   
    try:
        with codecs.open(fname, 'rb', encoding='iso-8859-1') as f:
            lines = f.readlines()
    except:
        print('This files is probably not iso-8859-1 encoded. Testing with chardet...')
        guess_encoding(fname)
        sys.exit(0)
    
    ini = 0
    for n in range(len(lines)):
        if lines[n].startswith('mode') or 'ox/red' in lines[n]:
            ini = n; break
    print(ini)
    
    time_l = lines[ini].split('\t')[8]
    potential_l = lines[ini].split('\t')[10]
    current_l = lines[ini].split('\t')[11]
    dt, eV, imA = np.empty(len(lines)-(ini+1)), np.empty(len(lines)-(ini+1)), np.empty(len(lines)-(ini+1))
    
    for n, l in enumerate(lines[(ini+1):]):
        cols =  l.split('\t')
        dt[n] = dtconv(cols[8])
        eV[n] = float(cols[10])
        imA[n] = float(cols[11])
    
    dt -= dt[0]
    return dt, eV, imA
#
#use np.loadtxt if you manually deleted the first 89 lines
#eV, imA = np.loadtxt(sys.argv[1], unpack=True, 
#                        usecols=(10,11), delimiter='\t')
#
#with open('Logs/MJ1-01-b_C01.mpt', 'r') as f:
#    l = f.readlines()
#dt = np.empty(len(l))
#for i in range(len(l)):
#    cols = l[i].split('\t')
#    dt[i] = dtconv(cols[8])
#dt -= dt[0]
#
    

    


if __name__ == '__main__':
    dt, eV, imA = eclog(sys.argv[1])
    app = QtGui.QApplication([])
    win = pg.GraphicsWindow()
    label = pg.LabelItem(justify = "right")
    win.addItem(label)
    plt = win.addPlot(row=1, col=0)
    proxy = pg.SignalProxy(plt.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
    
    plt.showGrid(True, True, 0.5)
    
    le = pg.ScatterPlotItem(x=dt, y=eV, pen=pg.mkPen('y'), symbol='o')
    li = pg.ScatterPlotItem(x=dt, y=imA/1000, pen=pg.mkPen('r'), symbol='o')
    plt.addItem(le); 
    plt.addItem(li)
    
    pg.QtGui.QApplication.processEvents()
    pg.QtGui.QApplication.processEvents()
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
